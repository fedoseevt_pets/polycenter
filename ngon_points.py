from typing import Tuple
import math
import cmath

from point import Point


def nearest_ngon_points(
	p1: Point,
	p2: Point,
	n: int,
) -> Tuple[Point]:
	p1 = p1.to_complex()
	p2 = p2.to_complex()
	angle = math.pi / n
	rotation = cmath.rect(1, angle)
	r2 = rotation * rotation

	v = p2 - p1
	p1_next = -v / r2 + p1
	p2_next = v * r2 + p2
	return Point.from_complex(p1_next), Point.from_complex(p2_next)


def opposite_ngon_point(
	p1: Point,
	p2: Point,
	n: int,
) -> Point:
	p1 = p1.to_complex()
	p2 = p2.to_complex()
	angle = math.pi / n
	rotation = cmath.rect(1, angle)

	v = p2 - p1
	if n % 2 == 0:
		r2 = rotation * rotation
		c = v / (1 - r2)
		opp = 2*c - v/2
		return Point.from_complex(p1 + opp)
	return Point.from_complex(p1 + v/(1 - rotation))



