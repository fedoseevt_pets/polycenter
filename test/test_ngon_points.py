import cmath

from point import Point

import unittest

from ngon_points import (
	nearest_ngon_points, opposite_ngon_point
)


class TestPointsGeneration(unittest.TestCase):

	def assertAlike(self, n1, n2):
		PRECISION = 12
		if isinstance(n1, complex) or isinstance(n2, complex):
			n1 = complex(
				round(n1.real, PRECISION),
				round(n1.imag, PRECISION)
			)
			n2 = complex(
				round(n2.real, PRECISION),
				round(n2.imag, PRECISION)
			)
		else:
			n1 = round(n1, PRECISION)
			n2 = round(n2, PRECISION)
		self.assertEqual(n1, n2)

	def test_nearest_ngon_points(self):
		p1, p2 = Point(0, 0), Point(1, 0)
		p1, p2 = nearest_ngon_points(p1, p2, 3)
		self.assertAlike(
			p1.to_complex(), cmath.rect(1, cmath.pi/3)
		)
		self.assertAlike(
			p2.to_complex(), cmath.rect(1, cmath.pi/3)
		)

		p1, p2 = Point(0, 0), Point(1, 0)
		p1, p2 = nearest_ngon_points(p1, p2, 4)
		self.assertAlike(
			p1.to_complex(), 0 + 1j
		)
		self.assertAlike(
			p2.to_complex(), 1 + 1j
		)

		p1, p2 = Point(0, 0), Point(1, 0)
		p1, p2 = nearest_ngon_points(p1, p2, 8)
		self.assertAlike(
			p1.to_complex(), cmath.rect(1, 3 * cmath.pi/4)
		)
		self.assertAlike(
			p2.to_complex(), cmath.rect(1, cmath.pi/4) + 1
		)

	def test_opposite_ngon_point(self):
		p1, p2 = Point(0, 0), Point(1, 0)
		p = opposite_ngon_point(p1, p2, 3)
		self.assertAlike(
			p.to_complex(), cmath.rect(1, cmath.pi/3)
		)

		p1, p2 = Point(0, 0), Point(1, 0)
		p = opposite_ngon_point(p1, p2, 4)
		self.assertAlike(
			p.to_complex(), 0.5 + 1j
		)

		p1, p2 = Point(0, 0), Point(1, 0)
		p = opposite_ngon_point(p1, p2, 8)
		self.assertAlike(
			p.to_complex(), 0.5 + (1 + 2**0.5) * 1j
		)



