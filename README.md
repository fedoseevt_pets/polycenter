# Polycenter

## What is polycenter

Triangle polycenter bases on n-gons is something like this
(instersection point):

![based on 5-gons](img/polycenter_5.png)

![based on 6-gons](img/polycenter_6.png)

## What this program do

This plogram draw polycenters based on different n-gons up to 100-gon.

## How to run this program

Make virtual environment:

```
python -m venv venv
. venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

Run:

```
python main.py
```




