from typing import Self, Optional

from point import Point


class Line():

	@classmethod
	def x_eq(cls, n: float):
		return cls(1, 0, -n)

	@classmethod
	def y_eq(cls, n: float):
		return cls(0, 1, -n)

	@classmethod
	def through(cls, a: Point, b: Point):
		dx = a.x - b.x
		dy = a.y - b.y
		y_coef = dx
		x_coef = -dy
		free_coef = -(a.x * x_coef + a.y * y_coef)
		return cls(x_coef, y_coef, free_coef)

	def __init__(
		self,
		x_coef: float,
		y_coef: float,
		free_coef: float,
	) -> None:
		if x_coef == 0 and y_coef == 0:
			raise ValueError('Line is variable independent!')
		self._a = x_coef
		self._b = y_coef
		self._c = free_coef
		self._normalize()

	def intersect(self, other: Self) -> Optional[Point]:
		self._normalize()
		other._normalize()
		if (
			self._a == other._a 
			and self._b == other._b
		):
			return None
		x = (
			(other._b * self._c - self._b * other._c)
			/ (other._a * self._b - self._a * other._b)
		)
		if self._b != 0:
			y = -(self._c + self._a * x) / self._b
		elif other._b != 0:
			y = -(other._c + other._a * x) / other._b
		else:
			return None
		return Point(x, y)

	def _normalize(self) -> None:
		norm_coef = min(c for c in (self._a, self._b) if c != 0)
		self._a /= norm_coef
		self._b /= norm_coef
		self._c /= norm_coef





