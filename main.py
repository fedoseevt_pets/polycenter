import pygame

from base import main
from setup import config as setup_config
from config import config as app_config
from app import App


if __name__ == '__main__':
	pygame.init()
	app = App(**app_config)
	main(app, setup_config)
	pygame.quit()


