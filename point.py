from typing import Self


class Point():

	@classmethod
	def from_complex(cls, z: complex):
		return cls(z.real, z.imag)

	def __init__(self, x: float, y: float) -> None:
		self._z = complex(x, y)

	@property
	def x(self) -> float:
		return self._z.real

	@property
	def y(self) -> float:
		return self._z.imag

	def midpoint(self, other: Self) -> Self:
		return Point.from_complex(
			(self.to_complex() + other.to_complex()) / 2
		)

	def distanse(self, other: Self) -> float:
		return abs(self.to_complex() - other.to_complex())

	def to_complex(self) -> complex:
		return self._z

	def __complex__(self) -> complex:
		return self._z

	def __eq__(self, other: Self) -> bool:
		return self.to_complex() == other.to_complex()

	def __repr__(self) -> str:
		return f'Point({self.x}, {self.y})'

	def __hash__(self) -> int:
		return id(self)



