config = {
	'back_color': 'wheat',

	'polycenter_count': 100,

	'topleft': None,
	'scale_per_pixel': 0.01,
	'scale_zoom_ratio': 1.1,

	'point_draw_radius': 4,
	'captured_point_color': 'cyan',
	'point_abc_color': 'tomato',
	'point_base_color': 'plum',

	'segment_draw_width': 3,
	'segment_base_color': 'gray',

	'point_capture_distance': 10,
}
