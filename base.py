from typing import Dict, Any

import pygame


default = {
	'RESOLUTION': (640, 480),
	'FLAGS': 0,
	'FPS': 30,
	'CAPTION': 'Polycenter',
	'ICON': None,
}


def setup(config: Dict[str, Any]) -> None:
	resolution = config.get('RESOLUTION', default['RESOLUTION'])
	flags = config.get('FLAGS', default['FLAGS'])
	caption = config.get('CAPTION', default['CAPTION'])
	icon = config.get('ICON', default['ICON'])

	pygame.display.set_mode(resolution, flags)
	if caption is not None:
		pygame.display.set_caption(caption)
	if icon is not None:
		pygame.display.set_icon(
			pygame.image.load(icon).convert_alpha()
		)


class BaseApp():

	def __init__(self, back_color=None) -> None:
		self._back_color = back_color

		self._screen: pygame.Surface = None

	def init(self) -> None:
		self._screen = pygame.display.get_surface()

	def event(self, event: pygame.event.EventType) -> None:
		pass

	def step(self, dt: float) -> None:
		if self._back_color is not None:
			self._screen.fill(self._back_color)

	def quit(self) -> None:
		pass


def main(app: BaseApp, config: Dict[str, Any]) -> None:
	setup(config)
	fps = config.get('FPS', default['FPS'])
	clock = pygame.time.Clock()

	app.init()
	while True:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				return app.quit()
			app.event(event)
		dt = clock.tick(fps) / 1000
		app.step(dt)
		pygame.display.flip()





