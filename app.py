from typing import Tuple, Iterator

import pygame

from base import BaseApp
from engine import Engine
from point import Point
from line import Line


class App(BaseApp):

	def __init__(self,
		polycenter_count: int = 100,
		topleft: Tuple[float] = None,
		scale_per_pixel: float = 0.01,
		scale_zoom_ratio: float = 1.1,
		point_draw_radius: int = 3,
		captured_point_color = 'cyan',
		point_abc_color = 'tomato',
		point_base_color = 'plum',
		segment_draw_width: int = 2,
		segment_base_color = 'gray',
		point_capture_distance: int = 10,
		**kwargs,
	) -> None:
		super().__init__(**kwargs)

		self._polycenter_count = polycenter_count

		self._topleft: Tuple[float] = topleft
		self._scale_per_pixel: float = scale_per_pixel
		self._scale_zoom_ratio: float = scale_zoom_ratio

		self._point_draw_radius = point_draw_radius
		self._point_abc_color = point_abc_color
		self._point_base_color = point_base_color
		self._captured_point_color = captured_point_color

		self._segment_draw_width = segment_draw_width
		self._segment_base_color = segment_base_color

		self._point_capture_distance = point_capture_distance
		self._is_point_captured: bool = False
		self._is_background_captured: bool = False
		self._captured_point: str = None

		self._engine: Engine = None
		self._view_size: Tuple[float] = None

		self._abc_triangle: Tuple[Point] = None

	def init(self) -> None:
		super().init()

		self._engine = Engine()

		self._update_view_size()
		if self._topleft is None:
			self._topleft = (-self._w_scale/2, self._h_scale/2)
		self._x0, self._y0 = self._topleft

		self._abc_triangle = {
			'A': Point(0, self._y0 * 0.8),
			'B': Point(-self._x0 * 0.7, -self._y0 * 0.3),
			'C': Point(self._x0 * 0.4, -self._y0 * 0.6),
		}

		for label, point in self._abc_triangle.items():
			self._engine.place_point(label, point)
		self._engine.draw_polygon(*self._abc_triangle)

		for i in range(self._polycenter_count):
			self._engine.draw_polycenter(
				*self._abc_triangle, 3 + i,
			)

	def step(self, dt: float) -> None:
		super().step(dt)
		self._draw_segments()
		self._draw_points()

	def event(self, event: pygame.event.EventType) -> None:
		if event.type == pygame.MOUSEBUTTONDOWN:
			if event.button == 4: # Mouse wheel backward
				self._event_zoom(event.pos, -1)
			if event.button == 5: # Mouse wheel forward
				self._event_zoom(event.pos, 1)
			if event.button == 1:
				self._event_capture(event.pos)
		if event.type == pygame.MOUSEBUTTONUP:
			if event.button == 1:
				self._event_uncapture()
		if event.type == pygame.MOUSEMOTION:
			self._event_capture_move(event.rel)

	def _event_capture(self, click_pos: Tuple[int]) -> None:
		click_pos = complex(*click_pos)
		for label, point in self._abc_triangle.items():
			point_pos = complex(*self._scale_to_pixel(point))
			distance = abs(click_pos - point_pos)
			if distance <= self._point_capture_distance:
				self._is_point_captured = True
				self._captured_point = label
				break
		else:
			self._is_background_captured = True

	def _event_uncapture(self) -> None:
		if self._is_point_captured:
			self._is_point_captured = False
			self._captured_point = None
		if self._is_background_captured:
			self._is_background_captured = False

	def _event_capture_move(self, rel: Tuple[int]) -> None:
		shift = complex(
			rel[0] * self._scale_per_pixel,
			-rel[1] * self._scale_per_pixel,
		)
		if self._is_point_captured:
			self._event_capture_move_point(shift)
		if self._is_background_captured:
			self._event_capture_move_background(shift)

	def _event_capture_move_point(self, shift: complex) -> None:
		point = self._abc_triangle[self._captured_point]
		point = Point.from_complex(point.to_complex() + shift)
		self._engine.move_point(self._captured_point, point)
		self._abc_triangle[self._captured_point] = point

	def _event_capture_move_background(self, shift: complex) -> None:
		self._topleft = (
			self._x0 - shift.real,
			self._y0 - shift.imag,
		)
		self._x0, self._y0 = self._topleft

	def _event_zoom(self, pos: Tuple[int], direction: int) -> None:
		ratio = self._scale_zoom_ratio ** direction
		center = Point(*self._pixel_to_scale(pos))

		center = center.to_complex()
		v = complex(*self._topleft) - center
		topleft = center + v * ratio
		self._topleft = topleft.real, topleft.imag

		self._scale_per_pixel *= ratio
		self._update_view_size()
		self._x0, self._y0 = self._topleft

	def _draw_points(self) -> None:
		for point in self._engine.points:
			color = self._get_point_color(point)
			pos = tuple(self._scale_to_pixel(point))
			pygame.draw.circle(
				self._screen, color, pos,
				self._point_draw_radius,
			)

	def _get_point_color(self, point: Point):
		if point is self._abc_triangle.get(self._captured_point):
			return self._captured_point_color
		if point in self._abc_triangle.values():
			return self._point_abc_color
		return self._point_base_color

	def _draw_segments(self) -> None:
		for vertices in self._engine.segments:
			vertices = map(self._engine.get_point, vertices)
			vertices = map(self._scale_to_pixel, vertices)
			vertices = map(tuple, vertices)
			pygame.draw.line(
				self._screen, self._segment_base_color,
				*vertices,
				self._segment_draw_width,
			)

	def _update_view_size(self):
		self._w_pixel, self._h_pixel = self._screen.get_rect().size
		self._w_scale = self._w_pixel * self._scale_per_pixel
		self._h_scale = self._h_pixel * self._scale_per_pixel

	def _scale_to_pixel(self, point: Point) -> Iterator[float]:
		yield self._w_pixel * (point.x - self._x0) / self._w_scale
		yield self._h_pixel * (self._y0 - point.y) / self._h_scale

	def _pixel_to_scale(self, pos: Tuple[float]) -> Iterator[float]:
		yield pos[0] / self._w_pixel * self._w_scale + self._x0
		yield -(pos[1] / self._h_pixel * self._h_scale - self._y0)




