from typing import Set, Dict, Tuple, Iterator
from itertools import combinations

from point import Point
from line import Line
from ngon_points import nearest_ngon_points, opposite_ngon_point


class Engine():

	def __init__(self):
		self._points: Dict[str, Point] = {}
		self._fixed_points: Set[Point] = set()
		self._segments: Set[Tuple[str]] = set()

		self._orders = []

	@property
	def points(self) -> Iterator[Point]:
		yield from self._points.values()
		yield from self._fixed_points

	@property
	def segments(self) -> Iterator[Tuple[str]]:
		yield from self._segments

	def place_point(self, label: str, p: Point) -> None:
		if label in self._points:
			return
		self._points[label] = p

	def get_nearest_point(self, p: Point) -> str:
		return min(
			self._points,
			key=lambda label:
				p.distance(self._points[label]),
		)

	def move_point(self, label: str, new_place: Point) -> None:
		self._points[label] = new_place
		self._update()

	def get_point(self, label: str) -> Point:
		return self._points[label]

	def draw_polygon(self, *labels: str) -> None:
		if len(labels) < 3:
			return

		for i in range(len(labels)):
			label_1 = labels[i]
			label_2 = labels[i - 1]
			if (
				label_1 not in self._points
				or label_2 not in self._points
			):
				continue
			self._segments.add((label_1, label_2))

	def draw_polycenter(
		self, p1: str, p2: str, p3: str, n: int,
	) -> None:
		if (
			p1 not in self._points
			or p2 not in self._points
			or p3 not in self._points
		):
			return
		self._orders.append(('polycenter', p1, p2, p3, n))
		self._update()

	def _update(self) -> None:
		self._fixed_points.clear()
		for order_type, *order_spec in self._orders:
			if order_type == 'polycenter':
				self._order_draw_polycenter(
					*order_spec,
				)

	def _order_draw_polycenter(
		self, p1: str, p2: str, p3: str, n: int,
	) -> None:
		p1, p2, p3 = map(self.get_point, (p1, p2, p3))
		(
			p1_opposite, p2_opposite, p3_opposite,
			p1_neighbor, p2_neighbor, p3_neighbor,
		) = self._get_points_for_polycenter(p1, p2, p3, n)
		'''
		for p in self._get_points_for_polycenter(p1, p2, p3, n):
			self._fixed_points.add(p)
			'''

		line1 = Line.through(p1_opposite, p1_neighbor)
		line2 = Line.through(p2_opposite, p2_neighbor)
		line3 = Line.through(p3_opposite, p3_neighbor)

		self._place_all_intersections(line1, line2, line3)

	def _place_all_intersections(self, *lines: Line):
		for line1, line2 in combinations(lines, 2):
			point = line1.intersect(line2)
			if point is None:
				continue
			self._fixed_points.add(point)

	def _get_points_for_polycenter(
		self, p1: Point, p2: Point, p3: Point, n: int
	) -> Iterator[Point]:
		yield opposite_ngon_point(p2, p3, n)
		yield opposite_ngon_point(p3, p1, n)
		yield opposite_ngon_point(p1, p2, n)

		p1_1, p2_2 = nearest_ngon_points(p1, p2, n)
		p2_1, p3_2 = nearest_ngon_points(p2, p3, n)
		p3_1, p1_2 = nearest_ngon_points(p3, p1, n)
		yield p1_1.midpoint(p1_2)
		yield p2_1.midpoint(p2_2)
		yield p3_1.midpoint(p3_2)



